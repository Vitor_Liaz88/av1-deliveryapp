import { Food } from "./food.model";

export class Section{
  id?:string;
  title?:string;
  food?: Food[];
  description?:string;
}
