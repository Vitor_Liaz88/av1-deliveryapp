import { Admin } from "./admin.model";
import { Food } from "./food.model";
import { User } from "./user.model";

export class Delivery{
  public price?: number;
  public itens?: Food[];
  public client?: User;
  public responsible?: Admin
}
