enum PaymentType{
  credit,
  debit,
  money,
  pix,
  check
}

export class Payment{
  public paymentType?: string;
  public emitedAt?: Date
}
