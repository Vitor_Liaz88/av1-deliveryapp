import { RestaurantReference } from "./restaurantReference.model";

export class Food {
  public id!:string;
  public title!:string;
  public category!: string;
  public price!: number;
  public description!: string;
  public observation!: string;
  public qtd!: number;
  public avaliableRestaurant!: RestaurantReference[];
  public aditional!: string;
  public inPromo!: boolean;
}
