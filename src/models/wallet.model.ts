export class Wallet{
  public id: string;
  public account: string;
  public revenues: number;
  public owing: number;
  public registry: string;
}
