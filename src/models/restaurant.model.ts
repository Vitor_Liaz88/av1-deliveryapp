import { Section } from "./section.model";


export class Restaurant{
  public id!: string;
  public name!:string;
  public ranking!:number;
  public deliveryPrice!: number;
  public category!: string;
  public menu!: Section[];
  public state!:string;
  public city!: string;
  public street!: string;
  public num!: number; 
  public createdAt!:Date;
  public upddatedAt!:Date;
  
  constructor(){}

  createCoupons(){

  }
  createNewSection(){

  }
  openSection(){

  }
  createNewFood(){

  }
  getPriceBalance(){
    
  }
}
