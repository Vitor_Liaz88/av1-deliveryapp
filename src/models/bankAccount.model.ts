export class BankAccount{
  public number?: string;
  public cardNumber?: number;
  public safeNumber?: number;
  public hodelr?: string;
  public createdAt?: string;
  public updatedAt?: string;
}
