export class Coupon{
  public category?: string;
  public value?: number;
  public validity?: Date
}
