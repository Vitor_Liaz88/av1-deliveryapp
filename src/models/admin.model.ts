import { BankAccount } from "./bankAccount.model";
import { RestaurantReference } from "./restaurantReference.model";
import {User} from "./user.model";
//TODO CREATE Restaurant

export class Admin extends User{
  public id!:string;
  public restaurant!: RestaurantReference;
  public revenues!: number;
  public expenses!: number;
  public qtdRequestes!:number;
  public requestesFailed!:number;
  public monthlyBalance!: number;
  public boostPromo!: boolean;
  public bankAccount!: BankAccount;
  
  constructor(){
    super();
  }

  restaurantRegister(){
    this.restaurant
  };

  openRestaurant():void{
    "Abrir Restaurant"
  }
  closeRestaurant():void{
    "Fechar Restaurant"
  }
  deleleRestaurant():void{
    "Deletar Restaurant"
    "Deletar Referencia"
  }

  returnPayment():void{
    "Retornar Pagamento Ao Usuário"
  }
}
