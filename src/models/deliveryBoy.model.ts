import { RestaurantReference } from "./restaurantReference.model";
import { User } from "./user.model";
import { Wallet } from "./wallet.model";
//CREATE CLASS WALLET

enum CityRegion{
  North,
  East,
  South,
  West,
}

export class DeliveryBoy extends User{
  public id!:string;
  public ranking!:number;
  public goals!:number;
  public cityRegion!: string;
  public deliveryFee!: number;
  public wallet!: Wallet;
  public vehicleType!: string;
  public vehicleRegistry!: string;
  public bondingRestaurant!: boolean;
  public restaurantReference!: RestaurantReference;
  public typeCNH!: string;
  public registryCNG!: string;

  getNewDelivery(cityRegion:string){
    
  }
  notifyEndDevilvery(){

  }
  routeDetails(){

  }
  requestsDetail(){

  }
  Payment(){

  }
}
