//TODO CREATE PAYMENT TYPE
//TODO CREATE NOTIFICATION
//TODO CREATE ORDER

import { Coupon } from "./coupon.model";
import { Payment } from "./payment.model";

export class User {
  public id?: string;
  public fullName?: string;
  public email!: string;
  public cpf!: string;
  public telephone!: string;
  public registry!: string;
  public paymentTypes!: Payment[];
  public coupons!: Coupon[];
  public imageProfile!: URL;
  public isActive!: boolean;
  public notification!: Notification;
  public createdAt!: Date;
  public updatedAt!: Date;
  public city!: string;
  public state!: string;
  public street!: string;
  public number: number;
  public password!: string;

  constructor() {}

  oderFood(food?:string, typePayment?:string) {
    return {
      client: this.fullName,
      addres: {
        city: this.city,
        state: this.state,
        street: this.street,
        number: this.number,
      },
      typePayment: typePayment,
      order: food,
    };
  };

  singUp(){
    const profile = {
      id: this.id,
      name: this.fullName,
      email: this.fullName,
      registry: this.registry,
    };
    return profile;
  };

  login(email:string, password:string){
    if(this.email === email || this.password === password){
      console.log('Você está logado no sistema');
    }
    else{
      console.log('Usuário ou senha invalidos');
    };
  };

  filterRestaurant(restaurant:string){
    //TODO create new List ...
    if(restaurant == 'List Restaurant'){
      console.log("ACHEI UM RESTAURANTE");
    };
  };

  filterFood(food:string){
    if(food === 'food list'){
      console.log("ACHEI UMA COMIDA");
    }
  }
}
