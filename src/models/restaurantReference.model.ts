export class RestaurantReference{
  public name?: string;
  public restaurantId?: string;
  public typeRestaurant?: string;
}
